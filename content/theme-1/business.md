## Stratégies et modèles d'affaires
*L'impact du commerce en ligne*

<video src="img/business-model.mp4" autoplay loop muted data-autoplay></video>

[Source](https://giphy.com/gifs/season-13-the-simpsons-13x18-xT5LMJRZ41LQmNIYDK/media)

-----

## Quelques définitions

- **Modèle d’affaires :** Décrit l'organisation, le fonctionnement et la rentabilité d'une entreprise.  
- **Modèle d’affaire de commerce électronique :** Intègre pleinement Internet, le Web et les plateformes mobiles.  
- **Plan d’affaires :** Document structurant le modèle d’affaires.  

**➡ Démonstration en classe**: Trousse BDC

-----

## Fondements économiques

* <u>Opportunité de marché</u>: Fait référence à un potentiel commercial et
financier disponible dans un marché qu’une entreprise souhaite compétitionner

* <u>Proposition de valeur</u>: Défini comment une entreprise remplit un ou des
besoins des clients.

* <u>Modèle de revenue</u>: Décri comment une entreprise produira des revenus,
des profits et retours sur investissement supérieur.

* <u>Environnement compétitif</u>: Fait référence aux autres compagnies qui
opèrent dans le même marché et qui vendent des produits similaires.

-----

## Fondements économiques (suite)

* <u>Équipe de gestion</u>: Employés d’une compagnie responsables de mettre en
œuvre le modèle d’affaires.

* <u>Développement organisationnel</u>: Plan qui décrit comment la compagnie
organisera le travail qui doit être accompli.

* <u>Stratégie de marché</u>: Plan qui regroupe et détaille comment une entreprise
entrera dans un nouveau marché et attirera de nouveau consommateur.

* <u>Avantages compétitifs</u>: Avantages qu’une entreprise possède lorsqu’elle est
en mesure de produire un produit supérieur ou offrir un prix inférieur
comparativement aux concurrents.

-----

## 5 modèles de revenues

* Publicité
* Inscription
* Frais de transaction
* Ventes
* Affiliation

Il est possible de combiner plusieurs modèles de revenues!

**➡ Exemples en classe**

-----

## Avantages de vendre en ligne

<div class="fragment">
  <ul>
    <li><b>Disponible partout et en tout temps</b></li>
    <li>Réduction des coûts (généralement)</li>
    <li>Potentiel de marché mondial</li>
    <li>Personnalisation des messages et services</li>
    <li>Contenus riches et interactifs</li>
    <li>Standards partagés par plusieurs pays</li>
    <li>...</li>
  </ul>
</div>

-----

## Impact sur la structure des entreprises

* L'industrie du voyage
* L'industrie des assurances
* L'industrie financière
* ...

**➡ Exemples en classe**

-----

## L'évolution d'amazon

Un outil intéressant:
[Wayback Machine](https://archive.org/web/)

**➡ Démonstration en classe**

-----

## L'impact en 10 ans
![](img/stats-market-share-1.jpg)

[Source](https://www.valuewalk.com/2016/12/extraordinary-size-amazon-one-chart/)

-----

## La naissance d'un géant
![](img/stats-market-share-2.jpg)

[Source](https://www.valuewalk.com/2016/12/extraordinary-size-amazon-one-chart/)

-----

## À quel prix?
![](img/amazon-workers.jpg)

**➡ Discussion en classe**:
[4:50 à 8:00 environ](https://www.netflix.com/watch/81070659?trackId=13752289&tctx=0%2C0%2Cec25e00c-10ca-47b9-94e8-9cf69672f893-22512670%2C%2C)

[Source](https://southpark.cc.com/)
