# IGE511
## Thème 1

Importance du commerce électronique dans les organisations et l’économie

----------

## Enchanté!

Mathieu Le Reste

mathieu.le.reste@usherbrooke.ca

🚫 Disponibilités professionnelles limitées, je suis disponible cette session avant et après le cours sur demande.  

----------

## Le cours

- 🌟 **Un mot : passion**  
  Ce cours a été conçu pour être inspirant et interactif.  

- 🎯 **Une formule allégée**  
  L’objectif est d’équilibrer la théorie et la pratique.  

- 🤝 **Votre participation : clé du succès**  
  Des discussions enrichies par vos idées et questions.  

- 🛠️ **Des cas réels, pas de problèmes “jouets”**  
  Nous analyserons des projets et situations actuels

----------

## Note sur la présentation

Cette présentation utilise la librairie web **[Reveal.js](https://github.com/hakimel/reveal.js)** pour une navigation interactive.  

🔗 Accédez au contenu ici : [ige511.com](https://ige511.com)  

📂 **Code source & exemples :**  
Disponible sur [GitLab](https://gitlab.com/mlereste/ige511-notes-cours).  

🖨️ **Impression ou sauvegarde PDF :**  
Pour une version imprimable, [cliquez ici](?print-pdf) (Google Chrome recommandé).  

💡 **Astuce navigation :**  
Utilisez les flèches du clavier pour naviguer ou la touche `ESC` pour afficher le plan.

----------

## Sources et remerciements

Un grand merci à **Paul Nguyen**, ancien chargé de ce cours, pour avoir partagé sa passion et ses ressources pédagogiques, qui servent de base à cette version améliorée.
