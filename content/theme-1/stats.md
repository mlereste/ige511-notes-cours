## Le commerce électronique
*Définition et quelques chiffres*

![](https://media.giphy.com/media/xT0xePyGsKplOK5dHG/giphy.gif)

[Source](https://giphy.com/gifs/adweek-shopping-amazon-xT0xePyGsKplOK5dHG)

-----

## Définition

<div class="fragment">
<blockquote>
"Ensemble des activités commerciales qui sont effectuées par l'entremise d'internet, incluant la promotion, l'achat et la vente en ligne de produits et services."
</blockquote>

[Source](http://www.granddictionnaire.com/ficheOqlf.aspx?Id_Fiche=8390684)
</div>

-----

## Que peut-on vendre?

<div class="fragment">
  <ul>
    <li>Biens: vêtements, électronique, etc.</li>
    <li>Services: SAAS, cellulaire, etc.</li>
    <li>Divertissement: musique, films, séries, etc.</li>
    <li>Alimentation: épicerie, préparation de repas, etc.</li>
    <li>La liste en longue...</li>
  </ul>
</div>

-----

## La pointe de l'iceberg

![](img/stats-iceberg.jpeg)

[Source](https://medium.com/@smartrac/the-deep-web-the-dark-web-and-simple-things-2e601ec980ac)

-----

## 1995 à 2000: l'invention

* Produits de détail de base
* Avantage concurrentiel aux *first mover*
* Moteurs de recherches limités
* Publicité simple et abordable

-----

## 2001 à 2006: la consolidation

* Ordinateurs personnels de plus en plus accessibles
* Passage du *technology driven* au *business-driven*
* Emphase sur la consolidation et l’extension des marques
* Introduction de services plus complexes (finance, voyage, etc.)
* Publicité, segmentation et ciblage plus avancés

-----

## 2006 à 2015: l'explosion

* Début du iPhone et appareils mobiles
* Début des médias sociaux
* Évolution rapide des technologies web
* Adoption rapide des technologies web
* Collecte et utilisation des données personnelles

-----

## 2016 à aujourd'hui: la révolution

* Perturbation d'industries majeures:<br>
hôtels, transports, divertissement, etc.
* Compétition
* Intelligence artificielle
* Personalisation
* Vie privée

-----

## Quelques exemples intéressants
- Fizz
- GoodFood
- Netflix
- Amazon Prime

À qui bénéficie le plus ces changements?

**➡ Démonstration en classe**

-----

## Québecois ayant acheté en ligne
![](img/stats-achat-2023.png)

[Source](https://transformation-numerique.ulaval.ca/enquetes-et-mesures/netendances/le-commerce-electronique-au-quebec-2023/)


-----

## Québecois ayant acheté en ligne en 2023
![](img/stats-achat-2023-2.png)

[Source](https://transformation-numerique.ulaval.ca/enquetes-et-mesures/netendances/le-commerce-electronique-au-quebec-2023/)


-----

## Évolution de la mobilité
![](img/stats-mobile-3.png)

[Source](https://cefrio.qc.ca/fr/enquetes-et-donnees/netendances2018-mobilite-au-quebec/)

-----

## Impacts de la mobilité

Cas réel: taux de conversion

**➡ Discussion en classe**
