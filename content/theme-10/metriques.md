## Métriques
*Quoi surveiller et analyser*

-----

## Session

À ne pas confondre avec un visiteur unique. Une session est "un groupe d'interactions effectuées par un utilisateur pendant une période données".

En d'autres mots, ce concept représente une visite d'un internaute sur un site web.

Un internaute peut effectuer plusieurs sessions (visites) sur un même site.

[Référence complémentaire](https://support.google.com/analytics/answer/2731565?hl=fr)

-----

## Taux de rebond

Le taux de rebond est égal aux sessions avec consultation d'une seule page divisées par l'ensemble des sessions.

En d'autres mots, c'est le pourcentage des sessions pour lesquelles un visiteur est arrivé sur une page du site et est reparti sans consulter d'autres pages.

À titre indicatif, la moyenne se situe à environ 40% selon les industries.

-----

## Durée moyenne session

Il s'agit du temps moyen passé sur le site par session.

À titre indicatif, la moyenne se situe à environ 2 minutes selon les industries.

-----

## Sources de trafic

Les sources d'où proviennent les sessions sur un site web. Par exemple:

* *Organic search*
* *Direct*
* *Referral*
* *Social*
* *Paid search*
* *Paid social*
* *Display*

-----

## Pages de sortie

Pages par où le trafic (session) quitte un site.

-----

## Pages d'entrée

Pages par où le trafic (session) entre sur un site.

-----

## Pages par session

## Sessions par page

La différence entre les deux?

-----

## Taux de conversion

Chaque site a une raison d'être, c'est-à-dire un objectif d'action(s) que les visiteurs devraient effectuer. Par exemple, dans le cas du commerce en ligne, il s'agit d'un achat.

Le taux de conversion est le pourcentage des sessions ayant rempli ces objectifs.
