## Outils
*Pratiques et intéressants*

-----

## Google Analytics

Sers à mesurer les visites sur un site web. Une référence dans l'industrie!

➡ Démonstration en classe

[Site web officiel](https://analytics.google.com/analytics/web/)

-----

## Hotjar

Sers à analyser le comportement des visiteurs via des cartes de chaleurs et des funnels.

➡ Démonstration en classe

[Site web officiel](https://www.hotjar.com/)

-----

## Agency Analytics

Agrégateur de données très pratique!

➡ Démonstration en classe

[Site web officiel](https://agencyanalytics.com/)

-----

## La liste est longue!

* [kissmetrics](https://www.kissmetricshq.com/)
* [Open Web Analytics](http://www.openwebanalytics.com/)
* [crazyegg](https://www.crazyegg.com/)
* [tableau](https://www.tableau.com/)
* ...
