## Entrepreneuriat
*Comment se lancer en affaire*

-----

## Type d'entreprises au Québec

* Entreprtise individuelle
* Société par actions (inc)
* Société en nom collectif (senc)
* Coopérative
* ...

[Référence complémentaire](http://www.registreentreprises.gouv.qc.ca/fr/demarrer/differentes-formes-juridiques/default.aspx)

-----

## Registraire des entreprises du Québec

Votre référence en affaire! Ce portail vous permet de consulter le registre public et d'immatriculer une entreprise.

➡ Démonstration en classe

[Site web officiel](http://www.registreentreprises.gouv.qc.ca/fr/default.aspx)

-----

## Comment immatriculer une entreprise?

Via le registraire des entreprises du Québec!

➡ Démonstration en classe

[Site web officiel](http://www.registreentreprises.gouv.qc.ca/fr/demarrer/immatriculer/default.aspx)

-----

## Comptabilité

Plusieurs outils ou services s'offrent à vous afin de faire votre comptabilité d'entreprise. Voici quelques exemples dont nous allons discuter:

* [Quickbooks](https://quickbooks.intuit.com/ca/)
* [Zoho](https://www.zoho.com/)
* [Wave](https://www.waveapps.com/)
* [Meslivrescomptables](https://meslivrescomptables.ca/)
* [Bench](https://bench.co/)
* [Padgett](https://padgettsherbrooke.ca/)

➡ Exemple d'états des résultats

-----

## Taxes

À quel moment doit-on percevoir les taxes? Comment les remettre au gouvernement et surtout à quelle fréquence?

➡ Discussion en classe

-----

## Salaire, dividendes et impôts

En tant qu'entrepreneur, doit-on prendre un salaire ou des dividendes? Quelle est la différence?

➡ Discussion en classe

-----

## Avocats et notaires

Quels sont leurs rôles? Les tarifs? À quel moment faire appel à eux?

➡ Discussion en classe

-----

## Exemples

Mon humble parcours et les leçons apprises.

➡ Discussion en classe
