## Aspects légaux
*Juridiction, lois et responsabilités*

-----

## Au Québec et au Canada

Tous les marchands du Québec (physiques ou en ligne) doivent se conformer aux normes et lois en vigueur au Québec et au Canada.

Ceci inclut notamment le Code civil du Québec, la loi de protection du consommateur du Québec, la loi sur la concurrence du Canada et les lois de l'impôt.

[Site web officiel](https://www.opc.gouv.qc.ca/)

-----

## Contrats numériques

La législation provinciale sur le commerce électronique reconnaît la validité juridique des renseignements et documents, notamment les contrats, communiqués par voie électronique. La législation impose une position neutre par rapport aux supports; elle reconnaît aux communications, documents, contrats et signatures électroniques la même valeur fonctionnelle que leurs équivalents écrits ou imprimés.

-----

## Office de la protection du consommateur

"Des consommateurs avertis et des commerçants responsables pour un marché plus équilibré".

Voilà en quelques mots la raison d'être de cet organisme gouvernemental. C'est en quelque sorte le chien de garde du gouvernement qui permet de s'assurer que les consommateurs ne sont pas floués et qu'ils puissent porter plainte contre un marchand.

[Site web officiel](https://www.opc.gouv.qc.ca/)

-----

## Exemples

Voici quelques informations que l'on devrait retrouver sur un site de commerce en ligne selon les industries (souvent dans le pied de page):

* Conditions d'utilisation
* Politique de confidentialité
* Politique de retour et échange
* Politique de livraison
* Modalités d'achat
* Politique sur le prix
* ...

➡ Démonstration en classe

-----

## Taxes et frais de douane

En règle général, vous devez vous conformer aux lois de l'impôt et percevoir les taxes sur vos ventes en ligne (si vous atteignez le volume de vente minimal prévu par la loi).

En revanche, si votre acheteur est basé à l'extérieur du Canada et que le bien est livré à l'étranger, vous ne devez pas percevoir les taxes, mais vous devez remplir une déclaration douanière afin de permettre à son pays de percevoir les frais de douanes applicables.

-----

## Avocat

Dans tous les cas, il est recommandé de consulter un avocat avant de débuter en commerce électronique.

Le prix d'une consultation est plus intéressant que les conséquence d'une non-conformité. N'oubliez pas, c'est votre devoir de vous informer; l'ignorance n'est pas une défense admissible.
