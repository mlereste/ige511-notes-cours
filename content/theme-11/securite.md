## Sécurité
*Paiements en ligne et normes*

-----

## Norme PCI

La norme de sécurité de l'industrie des cartes de paiement (Payment Card Industry) est établie par les principaux fournisseurs de cartes (ex: Visa, MasterCard, American Express, etc.) afin de définir des standards de sécurité et de protection des données.

Les marchands doivent se conformer à cette norme selon certaines conditions et paliers sans quoi les conséquences peuvent être importantes (explication en classe).

➡ Démonstration en classe

[Site web officiel](https://fr.pcisecuritystandards.org/minisite/env2/)

-----

## Comment la respecter?

Le processus afin de se faire certifier PCI est long, complexe et coûteux (exemple en classe).

C'est pourquoi il vaut mieux utiliser des fournisseurs (payment gateway) qui ont déjà fait ce processus. Voici quelques exemples:

* [Stripe](https://stripe.com/)
* [Paypal](https://www.paypal.com/)
* [Bambora](https://www.bambora.com/)
* [Shopify](https://www.shopify.ca/)

-----

## HTTPS

Nous avons vu que le protocole HTTPS était dorénavant primordial pour le SEO. Mais pourquoi?

HTTPS assure que la connexion entre deux parties est cryptée et qu'il s'agit des bons interlocuteurs. Comment?

➡ Démonstration en classe

-----

## Encryption par clés

Une méthode populaire afin de crypter (cryptage asymétrique) consiste à utiliser une clé publique pour le chiffrement et une clé privée pour le déchiffrement.

➡ Démonstration en classe

-----

## Certificat

Un certificat est un élément d'information qui prouve l'identité du propriétaire d'une clé publique. À l'instar d'un passeport, un certificat est une preuve reconnue de l'identité d'une personne. Les certificats sont signés et transmis de façon sécuritaire par un tiers de confiance appelé autorité de certification (AC).

[Let's Encrypt](https://letsencrypt.org/fr/)
