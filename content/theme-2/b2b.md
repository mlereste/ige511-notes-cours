## B2B
*Business to business*

-----

## Définition

<div class="fragment">
<blockquote>
Le commerce B2B implique des transactions complexes entre entreprises, souvent via des ixtranets ou sites web spécialisés.
</blockquote>
</div>

-----

## Quelques exemples

* Bureau en Gros Entreprises
* Fournisseurs et distributeurs
* SAAS spécialisés

**➡ Exemples en classe**
