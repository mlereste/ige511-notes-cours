## B2C
*Business to consumer*

-----

## Définition

<div class="fragment">
<blockquote>
Le commerce B2C permet aux consommateurs d'acheter, via internet, des produits ou services en petites quantités directement auprès d'entreprises."

</blockquote>
</div>

-----

## Quelques exemples

* Amazon, Walmart, Costco
* Simons, Poche & Fils, Hoaka Swimwear
* PMC Tire, BBQ Québec, JeSuisNaturel
* Bestbuy, Apple
* Netflix, Spotify

**➡ Exemples en classe**
