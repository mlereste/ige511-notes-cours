## C2C
*Consumer to consumer*

-----

## Définition

<div class="fragment">
<blockquote>
Le commerce C2C facilite les achats entre particuliers via des plateformes numériques.
</blockquote>
</div>

-----

## Quelques exemples

* Ebay
* Kijiji
* Airbnb
* Uber

**➡ Exemples en classe**
