## Local

-----

## Définition

<div class="fragment">
<blockquote>
Le commerce local se concentre sur l'engagement basé sur la proximité géographique des clients.
</blockquote>
</div>

-----

## Quelques exemples

* Groupon
* Shopico
* Tuango

**➡ Exemples en classe**
