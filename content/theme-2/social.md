## Social

-----

## Définition

<div class="fragment">
<blockquote>
Le commerce social intègre les transactions dans les plateformes de médias sociaux.
</blockquote>
</div>

-----

## Quelques exemples

* Facebook Marketplace
* Pinterest Buyable Pins
* Instagram Shopping

**➡ Exemples en classe**
