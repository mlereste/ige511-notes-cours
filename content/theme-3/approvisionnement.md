## Processus et chaînes d’approvisionnement

-----

## Définition

<div class="fragment">
<blockquote>

  <p>Une chaîne d'approvisionnement englobe trois fonctions:</p>

  <ul>
    <li>Fourniture de produits à un fabricant</li>
    <li>Processus de fabrication</li>
    <li>Distribution de produits finis au consommateur par un réseau de distributeurs et de détaillants</li>
  </ul>

  <p>Les entreprises qui participent aux différents stades de ce processus sont liées les unes aux autres par une chaîne d'approvisionnement.</p>
</blockquote>

<a href="https://www.supplychaincanada.com/fr/chaine-approvisionnement">Source</a>
</div>

-----

## Chaîne **multi-tier**

![](img/chaine-multi-tier.png)

Source: E-Commerce 2016 Business, Technology, Society. 12th Edition. Pearson, USA.

-----

## Chaîne **multi-tier** (suite)

![](img/chaine-multi-tier-2.png)

[Source](https://www.slideshare.net/kwatson/the-merging-of-planning-and-execution)

-----

## Processus d’approvisionnement B2B

* **Recherche**: catalogues, Internet, force de vente, brochures, téléphone, fax
* **Qualification**: recherche, historique de crédit, vérifications avec concurrents, recherche téléphonique
* Négociation**: prix, termes de crédit, qualité, délais, escompte
* **Achat / commande**: commander, initier et envoyer PO (purchase order)
* **Facturation**: réception du PO, système financier, système de production, envoi de facture, validation du PO, système entrepôt
* **Livraison**: système de suivi (envoi/livraison)
* **Paiement**: confirmation de livraison, vérification, envoi et paiement de facture

-----

## Défis dans la gestion de la chaîne d’approvisionnement

* **Visibilité**: capacité d’avoir vue sur les fournisseurs, commandes et logistique
* **Prévision de la demande**: informer les fournisseurs des demandes futures
* **Planification de la production**: informer les fournisseurs du calendrier de production
* **Gestion des commandes**: suivre les commandes faites aux fournisseurs
* **Gestion de la logistique**: gérer la logistique des partenaires basés sur le calendrier de production

-----

## Les tendances

* Production *just-in time*
* Processus *lean*
* *Dropshipping*
* Automatisation et robotisation

-----

## Dropshipping

Un cas intéressant: [Oberlo](https://www.oberlo.ca/)

**➡ Démonstration en classe**

-----

## Gestion d'inventaire et entrepôt automatisée

* RFID
* NFC

[Les entrepôts d'Amazon](https://www.youtube.com/watch?v=HSA5Bq-1fU4)

**➡ Discussion en classe**
