## Plateformes technologiques de commerce électronique B2B

-----

## EDI
*Electronic Data Interchange*

- Automatisation des échanges entre entreprises.  
- Réduction des coûts administratifs.  

![](img/edi-1.png)

[Source](https://www.edibasics.com/what-is-edi/)

-----

## EDI (suite)
*Electronic Data Interchange*

![](img/edi-2.png)

[Source](https://www.edibasics.com/what-is-edi/)

-----

## ERP
*Enterprise resource planning*

<div class="fragment">
  <blockquote>
    Progiciel qui permet de gérer l'ensemble des processus d'une entreprise en intégrant l'ensemble de ses fonctions, dont la gestion des ressources humaines, la gestion comptable et financière, l'aide à la décision, mais aussi la vente, la distribution, l'approvisionnement et le commerce électronique.
  </blockquote>

  - Gestion centralisée des processus internes.  
  - Exemple : SAP, Microsoft Dynamics.  

  [Source](http://www.granddictionnaire.com/ficheOqlf.aspx?Id_Fiche=8874062)
</div>

-----

## CRM
*Customer relationship management*

<div class="fragment">
  <blockquote>
    Solution logicielle dont l'objectif est d'attirer de nouveaux clients et de les fidéliser. Les logiciels de gestion de la relation client se répartissent en quatre grands modules : gestion du support client sur Internet, automatisation des fonctions de vente, automatisation du marketing, soutien et suivi des clients.
  </blockquote>

  - Fidélisation client et optimisation des ventes.  
  - Exemple : Salesforce, HubSpot.  

  [Source](http://www.granddictionnaire.com/ficheOqlf.aspx?Id_Fiche=506621)
</div>
