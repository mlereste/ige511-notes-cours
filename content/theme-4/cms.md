## Gestionnaires de contenu
*Content Management Systems (CMS)*

-----

## Histoire des CMS

![](img/cms-timeline.png)

-----

## 2 types de CMS

<blockquote>
  <b>Hébergés (SaaS)</b>
  <ul>
    <li>Plus rapide à mettre en ligne</li>
    <li>Permet de tester son concept rapidement</li>
    <li>Pas de soucis avec les serveurs (scaling)</li>
    <li>Coûts faibles au départ, mais à surveiller</li>
  </ul>
</blockquote>
<blockquote>
<b>À déployer</b>
  <ul>
    <li>Très flexible sur les modules et modifications</li>
    <li>Parfait pour les modèles d’affaires créatifs</li>
    <li>Solutions souvent open source</li>
  </ul>
</blockquote>

-----

## Wordpress

- Sortie: 27 mai 2003
- License: Open Source GNU
- Dernière version: 5.3.2
- Écosystème: PHP et MySQL
- Parts de marché: 61% / 17%
- Extensions: +55 000
- Coûts: NDD, hébergement et plugins

[Site officiel](https://wordpress.org/)

-----

## Joomla!

- Sortie: 22 septembre 2005
- License: Open Source GNU
- Dernière version: 3.9.14
- Écosystème: PHP et MySQL
- Parts de marché: 4,8%
- Extensions: +10 000
- Coûts: NDD, hébergement et plugins

[Site officiel](https://www.joomla.org/)

-----

## Shopify

- Sortie: Juin 2006
- License: SAAS propriétaire
- Écosystème: RestAPI, Liquid et GraphQL
- Parts de marché: 3,1% / 7%
- Extensions: +1 000
- Coûts: Frais mensuels, NDD et plugins

[Site officiel](https://www.shopify.com/)

-----

## Drupal

- Sortie: 15 janvier 2000
- License: Open Source GNU
- Dernière version: 8.8.1
- Écosystème: PHP et MySQL
- Parts de marché: 3%
- Extensions: +43 000
- Coûts: NDD, hébergement et plugins

[Site officiel](https://www.drupal.org/)

-----

## Magento

- Sortie: 31 août 2007
- License: Open Source et propriétaire
- Écosystème: PHP et MySQL
- Parts de marché: 1,5% / 20%
- Extensions: +5 000
- Coûts: Frais mensuels, NDD, hébergement et plugins

[Site officiel](https://magento.com/)

-----

## Autres CMS notables

- Squarespace (2,8%)
- Wix (2,2%)
- PrestaShop (1,2%)
- Typo3 (1%)
- OpenCart (0,8%)
- BigCommerce
- Concrete5

-----

## Analyse de vitesse sur mobile

![](img/cms-benchmark.png)

[Source](https://backlinko.com/page-speed-stats)

-----

## Plugins de commerce en ligne

- [wooCommerce](https://woocommerce.com/) (WordPress)
- [WP eCommerce](https://wpecommerce.org/) (Wordpress)
- [j2 store](https://www.j2store.org/) (Joomla!)
- [Drupal Commerce](https://drupalcommerce.org/) (Drupal)

-----

## Analyse et comparaison

**➡ Discussion en classe**

-----

## Installation de WordPress

**➡ Démonstration en classe**
