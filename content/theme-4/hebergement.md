## Architecture web et hébergement

-----

## Internet 101

**➡ Démonstration en classe**

-----

## Noms de domaine (NDD)

> Permet de traduire une adresse IP en une adresse plus facile pour un humain (alias).

Exemples de registraires:
- NameCheap
- GoDaddy

Organismes gouvernants:
- CIRA
- ICANN

**➡ Démonstration en classe**

-----

## Critères à considérer

- Fiabilité 24/7
- Maintenance
- Sécurité
- Backup
- Coûts

-----

## Types d'hébergement

- Mutualisé
- VPS
- Dédié
- Cloud

-----

## Hébergement mutualisé

- Plusieurs sites (utilisateurs) sur un même serveur
- Partage des ressources de la machine
- Solution gérée (*managed*)
- Aucun accès *root*
- Configuration générale et peu flexible
- Économique et simple (≈10$ par mois)
- Exemples: Hostpapa, Hostgator, Funio, etc.

-----

## Hébergement VPS

- Machine virtuelle
- Plusieurs machines virtuelles sur un même serveur
- Ressources limitées mais garanties
- Accès *root*
- Configuration sur mesure
- Relativement économique (≈20$ par mois)
- Exemples: DigitalOcean, Linode, Funio, etc.

-----

## Hébergement dédié

- Serveur physique
- Contrôle complet des ressources
- Accès *root*
- Configuration sur mesure
- Dispendieux (≈100$ par mois)
- Exemples: iWeb, OVH, etc.

-----

## Hébergement cloud

- Ressources sur demande
- Meilleure redondance
- *Scaling* plus facile
- Flexible (facturé à l'heure)
- Exemples: Amazon AWS, Google Cloud, Microsoft Azure, etc.

-----

## Hébergement PaaS

- Déploiement simple et rapide
- Meilleure redondance
- *Scaling* plus facile
- Flexible (facturé à l'heure)
- Exemples: Heroku, Netlify, etc.

-----

## Scaling

> **Évolutivité verticale (*vertical scaling*)**:
Fais référence à la capacité d’augmenter les performances des composantes individuelles. Ex: plus de processeurs, RAM, etc.

<br>

> **Évolutivité horizontale (*horizontal scaling*)**:
Fais référence à la capacité d’utiliser plusieurs ordinateurs pour partager la charge et bonifier les installations.

-----

## Serveurs web: OS

- Ubuntu Server (26,9%)
- CentOS (16,4%)
- Windows (4,3%)

[Source](https://www.datanyze.com/market-share/operating-systems/windows-server-market-share)

-----

## Serveurs web: plateformes

- Apache: *process-based*
- Nginx: *event-based*

> "Apache is like Microsoft Word, it has a million options but you only need six.
Nginx does those six things, and it does five of them 50 times faster than Apache." -Chris Lea.

-----

## Serveurs web: tendances

![](img/web-server-market-share.png)

[Source](https://news.netcraft.com/archives/2018/02/13/february-2018-web-server-survey.html)

-----

## Serveurs web: couches logicielles

- CPanel (80,4%)
- Plesk (18%)
- WHMCS (0,9%)

[Source](https://www.datanyze.com/market-share/hosting-control-panels/cpanel-market-share)

-----

## Survol de CPanel

**➡ Démonstration en classe**
