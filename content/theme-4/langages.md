## Standards et langages web

-----

## Survol des langages web

- HTML
- CSS
- JavaScript

Nous verrons en détail ces langages durant le thème 6.

-----

## W3C

> Le World Wide Web Consortium (W3C) est une communauté internationale où les membres, une équipe à plein temps, et le public travaillent ensemble pour développer les standards du web. La mission du W3C est de conduire le web à son plein potentiel.

[W3C Validation Service](https://jigsaw.w3.org/css-validator/)
