## Solutions de paiement en ligne

-----

## Survol des termes

- Titulaire de la carte (*Cardholder*)
- Marchand (*Merchant*)
- Compte marchand (*Merchant Account*)
- Passerelle de paiement (*Payment Gateway*)
- Banque émettrice (*Issuing Bank*)
- Banque réceptrice (*Acquiring Bank*)

-----

## Critères à considérer

- Facilité d'utilisation
- Compatibilité
- Prix et tarifs
- Sécurité
- Devises et internationalisation
- Soutien technique
- ...

-----

## Paypal

- Sortie: Décembre 1998
- Parts de marché: 59,5%
- Coûts: 2.9% + 0,30$

[Site officiel](https://www.paypal.com)

-----

## Stripe

- Sortie: 29 septembre 2011
- Parts de marché: 16,2%
- Coûts: 2.9% + 0,30$

[Site officiel](https://stripe.com)

-----

## Square

- Sortie: Février 2009
- Parts de marché: 2%
- Coûts: 2.9% + 0,30$

[Site officiel](https://squareup.com)

-----

## Analyse et comparaison

**➡ Discussion en classe**
