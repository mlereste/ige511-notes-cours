## Gestion de projet

*Commerce électronique*

![](https://media.giphy.com/media/xT1XGOGdyDrL2BTfxK/giphy.gif)

-----

## Relation client

![](img/gestion-projet.jpg)

[Source](https://www.facebook.com/125428807606111/photos/a.197991387016519/858300354318949/?type=1&theater)

-----

## La réalité

Selon le PMI, en 2018:

<blockquote>
  <ul>
    <li>30% des projets échouent</li>
    <li>40% des projets dépassent les coûts</li>
  </ul>
</blockquote>

Pour comparaison, en 2016:

<blockquote>
  <ul>
    <li>38% des projets échouent</li>
    <li>50% des projets dépassent les coûts</li>
  </ul>
</blockquote>

[Source](https://medium.com/crowdbotics/hips-dont-lie-15-project-management-stats-you-can-t-ignore-6f655060ef30)

-----

## Facteurs d'échec

![](img/facteurs-echec.png)

-----

## Les acteurs impliqués

- Client ou *product owner*
- Chargé de projet
- Analyste d'affaire
- Développeurs et intégrateurs
- Designers et graphistes
- Rédacteur
- Marketeur
- ...

La liste est longue!

-----

## Méthodes de gestion de projet

- Cascade
- Agile
- Scrum
- Kanban
- Scrumban
- ...

-----

## Logiciels de gestion de projet

- ClickUp
- Trello
- Jira
- Asana
- Monday
- Basecamp
- ...

-----

## Triangle de la gestion de projet

![](img/triangle-gestion-projet.jpg)

[Source](https://sites.google.com/site/projetpasapas/generalites)

-----

## Budget et coûts

> Typiquement, un site web de commerce électronique simple coûte entre 10 000$ et 100 000$ à produire en agence. Ceci dit, les coûts peuvent augmenter rapidement selon la complexité.

> À ceci il faut ajouter les coûts de marketing, les frais annuels et les frais d'exploitation.
