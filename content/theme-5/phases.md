## Phases de développement

*Commerce électronique*

-----

## Les points à considérer

- Étude de marché
- Analyse SEO
- Analyse UX
- Analyse marketing
- Aspects légaux et juridiques
- ...

-----

## Les questions à se poser

- Quelle est l’idée (visualisation du processus)
- Où est l’argent (modèle d’affaire et de revenu)
- Qui et où est l’audience cible
- Quelles sont les caractéristiques du marché
- Quels contenus le public peut-il s'attendre à obtenir sur le site web?
- D’où viendront les contenus
- Quels processus opérationnels seront intégrés aux fonctions offertes sur le site?
- Qu'en sera-t-il de sa maintenance continue?

-----

## Analyse FFOM

![](img/ffom.jpg)

[Source](https://www.bdc.ca/fr/articles-outils/strategie-affaires-planification/definir-strategie/pages/analyse-ffom-outil-simple-utiliser-planification-strategique.aspx)

-----

## Maquettes et prototypage

Tester et mesurer à peu de frais!

Un outil intéressant: [UX Pin](https://www.uxpin.com/)

-----

## Design Sprint

![](img/design-sprint.png)

[Source](https://www.gv.com/sprint/)
