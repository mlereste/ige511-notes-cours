## Recette (im)parfaite

*pour créer un commerce en ligne*

**➡ Exemple complet en classe**

-----

## 1. Trouver une idée

Qu'allez-vous vendre? Et pourquoi?

Est-ce une nouvelle idée ou une idée existante?

Quelle valeur créez-vous?

Êtes-vous intéressé ou passionné par cette idée?

-----

## 2. Analyser le marché

Est-ce que les consommateurs sont intéressés?

Quelle est la demande et la taille du marché pour cette idée?

Y a-t-il des concurrents ou des joueurs déjà présents?

➡ Idées: analyse des volumes de mots clés, analyse FFOM, etc.

-----

## 3. Preuve de concept (*POC*)

Quelle est la manière la plus simple et rapide de tester votre concept?

Avez-vous besoin de plus?

➡ Idées: *landing page* avec *opt-in*, Facebook marketplace ou Kijiji, etc.

-----

## 4. Choisir vos plateformes

Quel(s) nom(s) de domaine?

Quels CMS pour vos besoins?

Quels fournisseurs de paiements?

Avez-vous besoin d'outils supplémentaires (infolettre, marketing, ERP, CRM, etc.)?

➡ Idées: Capterra, G2 Crowd, *free trial*, discuter avec vos pairs, etc.

-----

## 5. Design

Qui est votre *persona* cible?

Comment réfléchit-il?

Quels appareils utilise-t-il?

➡ Idées: maquettes fils de fer, *blueprints*, Design Sprint, etc.

-----

## 6. Implémentation

Quels frameworks et technologies choisir?

Comment gérer le changement et le *versioning*?

➡ Idées: Gestion des sources, CI, environnement dev/stagging/prod, etc.

-----

## 7. Tests et QA

Est-ce que votre site est sécuritaire?

Est-ce que votre site fonctionne comme prévu?

Est-ce que votre site supporte un gros volume?

➡ Idées: Percy, PhantomJS, Pentests, etc.

-----

## 8. Mise en ligne

Indexation dans les moteurs de recherches?

Propagation DNS?

*Monitoring* des services?

➡ Idées: Sentry, etc.

-----

## 9. Promotion

Stratégie SEO et de contenu?

Campagnes publicitaires?

Où est votre *persona* cible?

➡ Idées: SEM, *display*, *retargeting*, IA, Similarweb, etc.

-----

## 10. Validation et amélioration

Cette étape devrait se faire au fur et à mesure du processus (*build, measure, learn*)!

Est-ce que votre site répond aux besoins et aux attentes des visiteurs?

Qui sont vos visiteurs? Est-ce le persona attendu?

Est-ce que vos acheteurs reviennent?

➡ Idées: Hotjar, Google Analytics, Google PageSpeed Insights, SEO scanners, etc.

-----

## Et voilà!

![](https://media.giphy.com/media/l4Jz3a8jO92crUlWM/giphy.gif)
