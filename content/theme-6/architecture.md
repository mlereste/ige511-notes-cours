## Architectures web
*Clients et serveurs*

-----

## Schéma système statique

![Schéma client serveur statique](img/client-serveur-statique.svg)

-----

## Schéma système dynamique

![Schéma client serveur dynamique](img/client-serveur-dynamique.svg)

-----

## Schéma système microservices

![Schéma client serveur microservices](img/client-serveur-microservices.svg)
