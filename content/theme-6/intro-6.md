# IGE511
## Thème 6

Utilisation de diverses technologies contributives

----------

## Note sur la présentation

Cette présentation utilise la librairie web **[Reveal.js](https://github.com/hakimel/reveal.js)** pour une navigation interactive.  

🔗 Accédez au contenu ici : [ige511.com](https://ige511.com)  

📂 **Code source & exemples :**  
Disponible sur [GitLab](https://gitlab.com/mlereste/ige511-notes-cours).  

🖨️ **Impression ou sauvegarde PDF :**  
Pour une version imprimable, [cliquez ici](?print-pdf) (Google Chrome recommandé).  

💡 **Astuce navigation :**  
Utilisez les flèches du clavier pour naviguer ou la touche `ESC` pour afficher le plan.

----------

## Note sur les sources

Tout au long du cours, je ferai référence à [Mozilla](https://developer.mozilla.org/fr/) et [W3schools](https://www.w3schools.com/), deux pionniers et sources historiques au niveau de la documentation du web et de ses standards.

----------

## La petite histoire du web

* Web 1.0: Connecte l'information - hyperliens, pages web, portails

* Web 2.0: Connecte les gens - courriels, réseaux sociaux

* Web 3.0: Connecte le savoir - Métadonnées (ex: JSON-LD), AI

* Web 4.0: Connecte les concepts - Big Data, Assistants (ex: Google home)

*Exemple de la patate en classe!*
