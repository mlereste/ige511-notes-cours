## AB Testing

*Aussi connu comme split testing*

-----

## Définition

Le AB testing consiste à créer 2 versions d'une page afin de tester l'efficacité d'un changement. La version A (référence) est montrée à 50% des visiteurs et la version B à l'autre moitié des visiteurs.

Attention aux *vanity metrics*!

**➡ Discussion en classe**

-----

## Comment tester rapidement

* Wireframes
* Blueprint
* Prototypes
* ...

-----

## Exemples concrets

Voici une présentation intéressante avec des cas concrets de AB testing. Serez-vous deviner les designs gagnants?

<iframe src="//www.slideshare.net/slideshow/embed_code/key/4T31iu7ieqrAT7" width="500" height="392" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen> </iframe>

[Source](https://www.slideshare.net/webanalystsinfo/guess-the-outcome-of-15-ab-tests)
