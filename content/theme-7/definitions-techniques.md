## Quelques définitions techniques

*Et techniques à considérer*

-----

## Average fold

Ligne virtuelle indiquant, en moyenne, le contenu que les utilisateurs voient sur une page sans avoir à défiler. Cette ligne varie selon les segments de visiteurs et les appareils qu'ils utilisent.

![](img/design-average-fold.png)

-----

## Lecture en Z

Il s'agit de la manière naturelle dont nous survolons une page web.

![](img/design-lecture-z.png)

-----

## L'impact de la vitesse

Encore une fois, la vitesse est très importante!

![](img/design-speed-conversion.svg)

[Source](https://www.cloudflare.com/learning/performance/more/website-performance-conversion-rates/)

-----

## Comment structurer l'information

Un site web bien structuré performe souvent mieux qu'un site simplement joli!

![](img/design-structure-information.png)

-----

## Personas

Une bonne technique pour connaître vos segments de clients.

![](img/design-personas.png)

[Source](https://boagworld.com/usability/personas/)

-----

## Comment amener l'information?

L'idée du funnel: sujet amené, posé et divisé.

![](img/design-funnel.png)

-----

## Les 7 C

* Contexte
* Contenu
* Communauté
* Personnalisation (customisation)
* Communication
* Connexion
* Commerce

-----

## Ergonomie

* Apprentissage: Comment est-il facile pour les utilisateurs d'accomplir des tâches de base la première fois qu'ils rencontrent le design?
* Efficacité: Une fois que les utilisateurs ont appris la conception, à quelle vitesse peuvent-ils effectuer des
tâches?
* Mémorisation: Quand les utilisateurs retournent à la conception après une période de ne pas l'utiliser, comment facilement peuvent-ils rétablir la compétence?
* Erreurs: Combien d'erreurs les utilisateurs font-ils, quelle est la gravité de ces erreurs, et comment peuventils facilement récupérer des erreurs?
* Satisfaction: Comment est-il agréable d'utiliser le design?

-----

## Erreurs fréquentes

* Trop d'information
* Espacement inadéquat
* Rupture du rythme
* Composantes hors norme
* ...

**➡ Discussion en classe**

-----

## Ne sous-estimez jamais les standards et les habitudes de vos visiteurs

Un interface ne doit pas réinventer la roue! Meilleur exemple: Google.

**➡ Discussion en classe**
