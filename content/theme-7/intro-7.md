# IGE511
## Thème 7

Design des interfaces Web en commerce électronique

----------

## Note sur la présentation

Cette présentation utilise la librairie web **[Reveal.js](https://github.com/hakimel/reveal.js)** pour une navigation interactive.  

🔗 Accédez au contenu ici : [ige511.com](https://ige511.com)  

📂 **Code source & exemples :**  
Disponible sur [GitLab](https://gitlab.com/mlereste/ige511-notes-cours).  

🖨️ **Impression ou sauvegarde PDF :**  
Pour une version imprimable, [cliquez ici](?print-pdf) (Google Chrome recommandé).  

💡 **Astuce navigation :**  
Utilisez les flèches du clavier pour naviguer ou la touche `ESC` pour afficher le plan.
