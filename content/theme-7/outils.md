## Quelques outils intéressants

*Prototypage, analyse et plus!*

-----

## Outils de prototypage populaires

* Adobe XD
* Sketch
* Canvas
* ...

**➡ Démonstration en classe**

-----

## Outils d'analyse populaires

* Hotjar
* Google Analytics Experiments
* Netlify Split Testing
* ...

**➡ Démonstration en classe**

-----

## Outils d'inspiration et de contenu

* Pexels
* Dribbble
* Adobe Color
* ...

**➡ Démonstration en classe**
