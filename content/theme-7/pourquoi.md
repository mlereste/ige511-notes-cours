## L'importance du contenu et du design

<img src="img/design-phone-input.gif" style="width: 400px;" /><br>
<img src="img/design-phone-input-2.gif" style="width: 400px;" />

[Source](https://qz.com/679782/programmers-imagine-the-most-ridiculous-ways-to-input-a-phone-number/)

-----

## Un bon design, c'est...

- 💎 **Valorisation des produits :** Augmente la valeur perçue.  
- 🛡️ **Rassurance :** Renforce la confiance et la sécurité des visiteurs.  
- 🌐 **Confiance :** Donne une image professionnelle.  

-----

## Exemple A: Bean Bags Montréal

![](img/design-beanbags-mtl.png)

[Source](https://www.beanbagmontreal.com/)

-----

## Exemple B: Fatboys

![](img/design-beanbags-fatboy.png)

[Source](https://fatboycanada.com/)

-----

## Lequel des 2 sites gagnerait votre confiance?

![](https://media.giphy.com/media/lKXEBR8m1jWso/giphy.gif)

-----

## Mais qu'est-ce qu'un bon design?

*En théorie*

Un bon design s'adapte et répond aux différents types de clients, leurs besoins, leurs attentes, leurs parcours et leurs plateformes.

-----

## Mais qu'est-ce qu'un bon design?

*En pratique*

Dans le cadre du commerce électronique, c'est une interface avec un haut taux de conversion et de ventes.

-----

## Dans les faits

On peut parler d'une interface, de la couleur d'un bouton ou d'un funnel de conversion pendant des heures, voir des jours...il y a d'ailleurs pas mal de chamans/gourous dans le domaine.

La solution? Réfléchir et produire un **MVP** qui sera **testé rapidement** afin d'**itérer** et de l'améliorer! Ramenons la **science** à l'avant-plan!
