## Les fondements du marketing

-----

## Le marketing mix (4P)

* Produit: ce que l'on veut vendre.
* Prix: la tarification et le positionnement de valeur.
* Promotion: l'offre valeur-proposition unique.
* Placement: l'endroit où l'on annonce.

-----

## Le mix média

> Le marketing consiste à mettre le bon produit, au bon prix, au bon endroit, au bon moment.

> Le mix média est la manière dont un budget publicitaire est ventilé.

**➡ Quels types de médias connaissez-vous?**

-----

## Répartition des investissements

![](img/investissements-medias.png)

[Source](https://www.emarketer.com/content/canada-digital-ad-spending-2019)

-----

## La force du multipoint

Le marketing est un outil très puissant, particulièrement lorsqu'il est utilisé sur plusieurs canaux en même temps.

Par contre, quelques défis de taille comme l'attribution ou la cannibalisation sont à surveiller.

**➡ Discussion en classe**
