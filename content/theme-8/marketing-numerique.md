## Marketing numérique

*La nouvelle ère du marketing!*

![](https://media.giphy.com/media/d2YZzTQvyoNYf9YI/giphy.gif)

-----

## Courriels

Il est essentiel de segmenter la base de données client et concevoir des listes d’envoi bien distinctes:

- Fréquence d’envoi
- Intérêts (produits, divertissement, ...)
- Valeurs du client (*lifetime value*)
- Préférence (promotion, événements, ...)
- ...

-----

## Courriels (suite)

Bonnes pratiques:

1. Implanter un système d’opt-in
2. Créer de la valeur pour le client (offre, événement, information privilégiée, ...)
3. Suivre l’audience et leurs actions
4. Personnaliser les communications
5. Créer une expérience fluide à travers les différents points de contact clients (accessibilité multiplateforme et parcours clients)
6. Faire des tests A/B

-----

## Courriels (suite)

Quelques fournisseurs:

- [Mailchimp](https://mailchimp.com/)
- [SendGrid](https://sendgrid.com/)
- [Vero](https://www.getvero.com/)
- ...

-----

## Courriels (suite)

Exemples de stratégies:

- Panniers abandonnés
- Suivi post-achat
- Offres, promotions, nouveautés, exclusivités
- ...

-----

## Courriels (suite)

Avec la loi C-28, une entreprise ne peut pas écrire à un consommateur sans son consentement.

- Consentement exprès: La personne a donné son consentement à recevoir vos communications. Par exemple, en remplissant un formulaire sur votre site web.
- Consentement tacite: Vous pouvez démontrer avoir une relation d'affaires avec cette personne.

Vous devez également offrir un système de gestion d'abonnement afin qu'un consommateur puisse retirer son consentement.

[Source](https://www.combattrelepourriel.gc.ca/eic/site/030.nsf/fra/accueil)

-----

## SEM

Le SEM consiste à payer afin d'améliorer son positionnement pour des mots-clés donnés.

Cette solution doit être considérée comme temporaire ou comme levier afin de concquérier de nouveaux marchés le temps que la stratégie de SEO soit effective.

-----

## SEM (suite)

Fonctionnement:

Le SEM fonctionne par enchère. Un annonceur cible une audience (ex: géographiquement), des mots-clés et enchérit ensuite au coût par clic (CPC) afin d'acheter de l'espace d'affichage parmi l'inventaire de recherches correspondant à ses critères.

-----

## Affichage programatique

L’affichage programmatique permet d’afficher des messages à des internautes, plus particulièrement des bannières, des vidéos et de l’audio.

Le plus grand inventaire (endroits où afficher) est Google avec environ 35% du marché, mais il ne faut pas négliger les autres inventaires disponiblescomme OpenX, Pubmatic, Rubicon, AppNexus, Index, etc. afin de générer plus d’opportunité et optimiser au maximum le budget et ROI.

-----

## Affichage programatique (suite)

L'affichage programmatique, un peu comme le SEM, fonctionne par enchère. Un annonceur cible une audience (ex: via un DSP) et enchérit ensuite au coût par mille (CPM) afin d'acheter de l'espace d'affichage parmi l'inventaire disponible correspondant à ses critères.

-----

## Affichage programatique (suite)

Quelques joueurs intéressants

* [Google Display](https://ads.google.com/intl/en_ca/home/campaigns/display-ads/)
* [AdRoll](https://www.adroll.com/)
* [Carbon](https://www.carbonads.net/)

-----

## Affichage programatique (suite)

Quelques techniques de ciblage:

* Géographique
* Démographique
* Comportemental
* Geofencing
* ...

-----

## Social

On peut voir la publicité sociale comme une sous-branche de l'affichage programmatique.

Les données que le réseau social a sur ses utilisateurs servent à améliorer le ciblage et le ROI.

-----

## Social (suite)

Quelques joueurs intéressants

* [Facebook Ads](https://www.facebook.com/business/ads)
* [Linkedin Ads](https://business.linkedin.com/marketing-solutions/ads)
* [Ad Espresso](https://adespresso.com/)

-----

## Suivi et tracking

Un des avantages du marketing numérique est la possibilité de mesurer le ROI et de faire de l'attribution avancée.

En général, un fournisseur fournit un code de suivi (ex: Pixel de Facebook) afin de mesurer et analyser le traffic découlant de vos campagnes.

**➡ Discussion en classe**

-----

## Forces

<div class="fragment">
  <ul>
    <li>ROI mesurable (mais pas garanti)</li>
    <li>Marché global</li>
    <li>Automatisation</li>
    <li>Personalisation (AB testing)</li>
    <li>Budgets flexibles</li>
    <li>...</li>
  </ul>
</div>

-----

## Faiblesses

<div class="fragment">
  <ul>
    <li>Expertise requise</li>
    <li>Intangible</li>
    <li>Lois et conditions d'utilisation</li>
    <li>Géants économiques (PIB)</li>
    <li>...</li>
  </ul>
</div>
