## Marketing traditionnel

*Encore d'actualité!*

![](https://media.giphy.com/media/g0nA46gxFb7Z6/giphy.gif)

-----

## Exemples de médias

* Télévision (Québecor, Bell, Radio-Canada, ...)
* Radio (Bell, Cogeco, ...)
* Imprimé (Journaux, revues, publipostage, ...)
* Affichage (Pattison, Outfront, Astral, ...)
* Affiliation et influenceurs
* ...

-----

## Comment fait-on du placement traditionnel?

En général:

1. Sélectionner les médias (mix média)
2. Contacter les représentants locaux de ces médias
3. Négocier les ententes
4. Fournir les créatifs
5. Suivis et gestion de dossier
6. ...

**➡ Discussion en classe**

-----

## Forces

<div class="fragment">
  <ul>
    <li>Notoriété locale accrue</li>
    <li>Économie locale (PIB)</li>
    <li>Copie physique (ex: flyer)</li>
    <li>Difficile à ignorer</li>
    <li>Accessible aux petites PME</li>
    <li>...</li>
  </ul>
</div>

-----

## Faiblesses

<div class="fragment">
  <ul>
    <li>Difficile de mesurer le ROI</li>
    <li>Processus humain: erreurs, agilité, etc.</li>
    <li>AB testing difficile</li>
    <li>Coûts parfois élevés</li>
    <li>Peu de personalisation</li>
    <li>...</li>
  </ul>
</div>

-----

## Modernisation en vue

- [Numéris](http://fr.numeris.ca/)
- [upfluence](https://www.upfluence.com/)
- DSP et DMP
