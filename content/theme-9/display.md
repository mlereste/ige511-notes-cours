## Affichage programmatique
*Bannières, vidéos et audio*

-----

## Définitions

L’affichage programmatique permet d’afficher des messages à des internautes, plus particulièrement des bannières, des vidéos et de l’audio.

Le plus grand inventaire (endroits où afficher) est Google avec environ 35% du marché, mais il ne faut pas négliger les autres inventaires disponiblescomme OpenX, Pubmatic, Rubicon, AppNexus, Index, etc. afin de générer plus d’opportunité et optimiser au maximum le budget et ROI.

-----

## Fonctionnement

L'affichage programmatique, un peu comme le SEM, fonctionne par enchère. Un annonceur cible une audience (ex: via un DSP) et enchérit ensuite au coût par mille (CPM) afin d'acheter de l'espace d'affichage parmi l'inventaire disponible correspondant à ses critères.

-----

## Quelques joueurs intéressants

* [Google Display](https://ads.google.com/intl/en_ca/home/campaigns/display-ads/)
* [Facebook Ads](https://www.facebook.com/business/ads)
* [Linkedin Ads](https://business.linkedin.com/marketing-solutions/ads)
* [AdRoll](https://www.adroll.com/)
* [Carbon](https://www.carbonads.net/)
* [Hubspot](https://www.hubspot.com/products/marketing)
* [Ad Espresso](https://adespresso.com/)

-----

## Outils intéressants

* [bannersnack](https://www.bannersnack.com/)
* [Google Web Designer](https://www.google.com/webdesigner/)
* [adbeat](https://www.adbeat.com/)

-----

## Techniques de ciblages

* Géographique
* Démographique
* Comportemental
* Geofencing
* ...
