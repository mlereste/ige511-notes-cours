## Moteurs de recherches
*Fonctionnement et description*

-----

## Les principaux joueurs

Compagnie | Parts de marché
--- | ---
Google | 90%
Bing | 4%
Yahoo! | 2.84%
Baidu | 0.56%

[Source: Statista](https://www.statista.com/statistics/216573/worldwide-market-share-of-search-engines/)

-----

## Comment fonctionnent-ils?

![Moteurs de recherche](img/search-engines.svg)
