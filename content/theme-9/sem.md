## Référencement payant
*SEM (Search Engine Marketing)*

-----

## Contexte

Le SEM consiste à payer afin d'améliorer son positionnement pour des mots-clés donnés.

Cette solution doit être considérée comme temporaire ou comme levier afin de concquérier de nouveaux marchés le temps que la stratégie de SEO soit effective.

-----

## Quelques exemples

➡  Démonstration en classe

-----

## Les principaux joueurs

Évidement, les principaux joueurs sont les moteurs de recherches ayant le plus de part de marché:

* [Google Ads](https://ads.google.com/intl/en_ca/home/)
* [Bing Ads](https://bingads.microsoft.com/)

➡ Démonstration en classe de Google Ads

-----

## Fonctionnement

Le SEM fonctionne par enchère. Un annonceur cible une audience (ex: géographiquement), des mots-clés et enchérit ensuite au coût par clic (CPC) afin d'acheter de l'espace d'affichage parmi l'inventaire de recherches correspondant à ses critères.

-----

## Outils intéressants

Afin de vous aider à identifier les mots-clés, choisir les enchères et analyser votre concurrence.

* [Google Keyword Planner](https://ads.google.com/intl/en_ca/home/tools/keyword-planner/)
* [Google Trends](https://trends.google.com/trends/?geo=US)
* [SEMRush](https://www.semrush.com/)
* [ahrefs](https://ahrefs.com/)
* [Similar Web](https://www.similarweb.com/)

-----

## Intelligence artificielle

L'IA est en plein essort dans le monde du SEM. En effet, elle permet d'optimiser les campagnes d'une manière qu'aucun humain ne peut égaler. Voici quelques plateformes:

* [Acquisio](https://www.acquisio.com/)
* [AdHawk](https://www.tryadhawk.com/)
* [Marin Software](https://www.marinsoftware.com/)
* [WordStream](https://www.wordstream.com/)

-----

## Exemples d'optimisation

Voici un exemple d'optimisations via l'AI et le machine learning. On peut voir que pour le même budget quotidien, le nombre de clics est passé de 12 par jours à 32 en moins de 20 jours!

![Clics](/img/ai-clics.png)

![CPC](/img/ai-cpc.png)
